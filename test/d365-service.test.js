'use strict'

const amqp = require('amqplib')

let _app = null
let _channel = null
let _conn = null
// let conf = { 
//   'tenant': 'https://bhaud365dev.crm6.dynamics.com',
//   'entitySchema': 'bookableresources',
//   'odataMethod' : '$select=name,msdyn_primaryemail,bookableresourceid'
// }

describe('D365 Service', function () {
  // process.env.OUTPUT_PIPES = 'Op1,Op2'
  // process.env.LOGGERS = 'logger1,logger2'
  // process.env.EXCEPTION_LOGGERS = 'exlogger1,exlogger2'
  // process.env.BROKER = 'amqp://guest:guest@127.0.0.1/'
  // process.env.INPUT_PIPE = 'demo.pipe.service'
  // process.env.CONFIG = JSON.stringify(conf)
  // process.env.OUTPUT_SCHEME = 'RESULT'
  // process.env.OUTPUT_NAMESPACE = 'RESULT'
  // process.env.ACCOUNT = 'demo account'

  let BROKER = process.env.BROKER
  let INPUT_PIPE = process.env.INPUT_PIPE

  before('init', () => {
    amqp.connect(BROKER)
      .then((conn) => {
        _conn = conn
        return conn.createChannel()
      }).then((channel) => {
      _channel = channel
    }).catch((err) => {
      console.log(err)
    })
  })

  after('terminate child process', function (done) {
    _conn.close()
    done()
  })

  describe('#start', function () {
    it('should start the app', function (done) {
      this.timeout(8000)
      _app = require('../app')
      _app.once('init', done)
    })
  })

  describe('#data', () => {
    it('should process the data and send back a result', function (done) {
      this.timeout(11000)
      //sample data
      var auth = {'token_type':'Bearer','scope':'user_impersonation','expires_in':'3599','ext_expires_in':'0','expires_on':'1516070969','not_before':'1516067069','resource':'https://bhaud365dev.crm6.dynamics.com','access_token':'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6Ino0NHdNZEh1OHdLc3VtcmJmYUs5OHF4czVZSSIsImtpZCI6Ino0NHdNZEh1OHdLc3VtcmJmYUs5OHF4czVZSSJ9.eyJhdWQiOiJodHRwczovL2JoYXVkMzY1ZGV2LmNybTYuZHluYW1pY3MuY29tIiwiaXNzIjoiaHR0cHM6Ly9zdHMud2luZG93cy5uZXQvNTA5ODQzMGMtYzIxMi00MTFiLTg3MjUtYzI1MTBhYWJkMWQ1LyIsImlhdCI6MTUxNjA2NzA2OSwibmJmIjoxNTE2MDY3MDY5LCJleHAiOjE1MTYwNzA5NjksImFjciI6IjEiLCJhaW8iOiJZMk5nWUFpb09Lb202cnRNZGYvMnhMSWpQNW4yeHUycDlTM3BTcHZZOGFCVm1iM1hPd29BIiwiYW1yIjpbInB3ZCJdLCJhcHBpZCI6ImQ5MmNiZDc4LTRmN2ItNDQxZi05MTY2LTk2MjUzYjRmYjRhNCIsImFwcGlkYWNyIjoiMSIsImZhbWlseV9uYW1lIjoiS2F0YWRhIiwiZ2l2ZW5fbmFtZSI6IkZyYW5rIiwiaXBhZGRyIjoiMTQuMTAyLjE2OC4yNiIsIm5hbWUiOiJGcmFuayBLYXRhZGEiLCJvaWQiOiI2YzM2ZmQzYS0wMzgzLTQ5YTMtYmYxYy1iNzI4MzQzZGFmOWMiLCJwdWlkIjoiMTAwMzdGRkVBMDQxNkIwQiIsInNjcCI6InVzZXJfaW1wZXJzb25hdGlvbiIsInN1YiI6IkpxNkxEdW5adFBCaE04cU93QUktYUpVcDVEaFkzckI0a0lpcnNhZ3BZX2siLCJ0aWQiOiI1MDk4NDMwYy1jMjEyLTQxMWItODcyNS1jMjUxMGFhYmQxZDUiLCJ1bmlxdWVfbmFtZSI6ImZyYW5rLmthdGFkYUBiYXJoZWFkLmNvbSIsInVwbiI6ImZyYW5rLmthdGFkYUBiYXJoZWFkLmNvbSIsInV0aSI6IjRtSk5GNThhQ2tHZFRYakFMUmN0QUEiLCJ2ZXIiOiIxLjAifQ.dq0Nsk4X8FAo-CSQYjU4HMg8QZ8EnLTXt1PWpqi_j1BsapTLTuyT2wJ77aw_lJHkwangKqepA5rVrce1Gm0Foy63tbHQTDrunkdsoZGaLeVqKBylEiIfao8tyvlBduqk_XY8g04qYEg-tuw-YcQd6t3suioDVboNwzxU1i4DVlQ4coXdeNecCf6BiccdhDqahjo8s86CjZrwkBZEHk6q2Sf6CpN_gDjBmwrkOPHC2jqIDYOJSA8GiYpSXsJjQ4jxSCQfJwcq7yR817m3n-5rPFltXnZnDOynCpU5wCA__swF9_5J9i1Ai2c9_pid2QjlW_IFZxk-t14BfxGuwPs3Nw','refresh_token':'AQABAAAAAABHh4kmS_aKT5XrjzxRAtHzcSXrQ_4KtMImQ_5BOxxvtWCBLV-3AuHhI3VEWp8wacd-9Hff1sf99WldIp8iw7h63NQ6W-ROxcVJkUWfqN8qgG0ScIq0f_nUW-3gdW-RPp9xrKeDXMzsa49LwICVceorGkpZQDkfZTvMJEg3ARDnBjuYMOViF9KQRvFsi_4XrRvssyRcIcnlwutQHDivuT9xx_XnOvGbO4VRIjEXMmy0phrqBwC5Rm5p1AUESr3Tlop-q9fjCmwzHWfLyMIhwYX7G-nHOhAS1kvtxEvLQAuQFqlm3EsnH-XoRbgHbjlegU7417CQSEL_VVRAZp54LDbGdtEq2rb0zl9E3MNCsex1RHJ9M5e7oBhY82SZZCX0SPVkvDqVbPlvLFN1oDhbeYAE4fCaMueoBZN9_K51dutsF1Ue_fI8T-ek-gHlYtvTD_La8ZYcvwSJYsw1e6xgoYxV5UIy875_3mYV5F7JkppJ0ZoJLnxu0UOyq0muprEInbAgZJ3GGK_KRRxQgsnE4_CmveEFXdeq8qQeyHdgObRqjKBR5BTvtKEFbJXVrmDiiHFhzZ0lY5g9GLXQ7nq6ImXhlUNRJjUUML_VBrXC-i2TP3ZOPkOfNos8AL6Y_cKaQexvWQ83rb4ixKgvnFkPBNZswwl4atZMbjmh6XK7HXbVx078hShjbbe_8MyhILKRuoBzNFN_JDuumqxbODbAnJKoa5IDULOQbNgO2rMys-QdAiAA'}

      _channel.sendToQueue(INPUT_PIPE, new Buffer(JSON.stringify(auth)))

      setTimeout(done, 10000)
    })
  })
})
